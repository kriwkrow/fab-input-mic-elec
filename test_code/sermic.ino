#define MIC_PIN 2
#define NUM_SAMPLES 50 // 100 was too much for ATtiny412

static unsigned char i; 
static unsigned char arr_lo[NUM_SAMPLES];
static unsigned char arr_hi[NUM_SAMPLES];

void setup() {
  Serial.begin(115200);
}

void loop() {
  
  // Send framing (as in Neil's code)
  Serial.write(1);
  delay(10);
  Serial.write(2);
  delay(10);
  Serial.write(3);
  delay(10);
  Serial.write(4);
  delay(10);

  // Populate buffer
  for(i = 0; i < NUM_SAMPLES; i++){
    int reading = analogRead(MIC_PIN);
    arr_lo[i] = (unsigned)reading & 0xff;
    arr_hi[i] = (unsigned)reading >> 8;  
  }
  // Send buffer
  for(i = 0; i < NUM_SAMPLES; i++){
    Serial.write(arr_lo[i]);
    Serial.write(arr_hi[i]); 
  }
}
