# Electret Mic Input Board

![Board](images/board.jpg)

A basic electret microphone capsule to serial board. Inspired by the [Adafruit Electret Microphone Amplifier MAX4466](https://www.adafruit.com/product/1063) with adjustable gain board. Instead of the MAX4466 amplifier chip the [Analog Devices AD8605](https://www.analog.com/media/en/technical-documentation/data-sheets/AD8605_8606_8608.pdf) op-amp from the [fab library](http://fab.cba.mit.edu/about/fab/inv.html) is used. There is no adjustable gain to reduce complexity.

## Making the Board

> **Important:** Make sure the [Fab KiCAD library](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) is installen and updated to its latest version.

The board was designed as semi-double-sided. What that means is that the back side of the copper covered clad is used for the ground plane only. That makes a few thing easier:

1. It simplifies the layout on the front side.
2. GND connections can connect to each other using a common ground plane.
3. No need to flip the PCB around and mill traces on the other side.

CopperCAM was used to generate paths needed for three operations:

1. Millng the traces.
2. Drilling the 0.8mm holes for the copper vias.
3. Cutting the board out.

You can use any other software, including Mods, to achieve the same with 0.4mm and 0.8mm flat end milling bits.

## Testing the Board

In the [test_code](test_code) directory you will find MCU code to be uploaded to the ATtiny412 chip and a Python script that plots incoming audio samples. Arduino IDE with [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) boards library was used to upload the MCU code. Python 3 has to be used to run the Python app.

You should know the device ID of the FTDI connection that you are using for the ready board. It should be similar to `/dev/ttyUSB0` on Linux and `COM0` on Windows. Open your terminal application and run the following command to launch the plotter. Replace `/dev/ttyUSB0` with your serial device identifier.

```
python3 sermic.py /dev/ttyUSB0
```

If everything works, you should see a window like this.

![serial plotter](images/microphone_test.jpg)

## Credits

Initial code by Neil Gershenfeld. The [ATtiny45 C code](http://academy.cba.mit.edu/classes/input_devices/mic/hello.mic.45.c) was adapted to Arduino style to be compatible with ATtiny412. The [serial plotter code](http://academy.cba.mit.edu/classes/input_devices/mic/hello.mic.45.py) is almost as-is in the original version.

## License

MIT license. Copyright (c) 2021 Krisjanis Rijnieks. See [LICENSE](LICENSE) file for more details.
