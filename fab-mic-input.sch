EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:OPAMP_AD8605 U2
U 1 1 6091ECA2
P 4750 5525
F 0 "U2" H 5094 5571 50  0000 L CNN
F 1 "OPAMP_AD8605" H 5094 5480 50  0000 L CNN
F 2 "fab:SOT-23-5" H 4750 5525 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/AD8605_8606_8608.pdf" H 4750 5725 50  0001 C CNN
	1    4750 5525
	1    0    0    -1  
$EndComp
$Comp
L fab:Microcontroller_ATtiny412_SSFR U3
U 1 1 6091F4C9
P 8825 3500
F 0 "U3" H 8295 3546 50  0000 R CNN
F 1 "Microcontroller_ATtiny412_SSFR" H 8295 3455 50  0000 R CNN
F 2 "fab:SOIC-8_3.9x4.9mm_P1.27mm" H 8825 3500 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 8825 3500 50  0001 C CNN
	1    8825 3500
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD J2
U 1 1 6091FC3D
P 3425 2525
F 0 "J2" H 3437 2750 50  0000 C CNN
F 1 "Conn_PinHeader_UPDI_1x02_P2.54mm_Horizontal_SMD" H 3437 2659 50  0000 C CNN
F 2 "fab:PinHeader_UPDI_01x02_P2.54mm_Horizontal_SMD" H 3425 2525 50  0001 C CNN
F 3 "~" H 3425 2525 50  0001 C CNN
	1    3425 2525
	1    0    0    -1  
$EndComp
$Comp
L fab:Conn_PinHeader_FTDI_1x06_P2.54mm_Horizontal_SMD J1
U 1 1 60920243
P 3425 1650
F 0 "J1" H 3462 2075 50  0000 C CNN
F 1 "Conn_PinHeader_FTDI_1x06_P2.54mm_Horizontal_SMD" H 3462 1984 50  0000 C CNN
F 2 "fab:PinHeader_FTDI_01x06_P2.54mm_Horizontal_SMD" H 3425 1650 50  0001 C CNN
F 3 "~" H 3425 1650 50  0001 C CNN
	1    3425 1650
	1    0    0    -1  
$EndComp
$Comp
L fab:Mic_Electret MK1
U 1 1 60920A28
P 1400 5900
F 0 "MK1" V 1354 6130 50  0000 L CNN
F 1 "Mic_Electret" V 1445 6130 50  0000 L CNN
F 2 "fab:PinHeader_1x02_P2.54mm_Horizontal_SMD" H 1400 5800 50  0001 C CNN
F 3 "~" H 1400 5900 50  0001 C CNN
	1    1400 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	8825 2800 8825 2575
$Comp
L fab:C C3
U 1 1 6095A919
P 7800 3175
F 0 "C3" H 7685 3129 50  0000 R CNN
F 1 "1uF" H 7685 3220 50  0000 R CNN
F 2 "fab:C_1206" H 7838 3025 50  0001 C CNN
F 3 "" H 7800 3175 50  0001 C CNN
	1    7800 3175
	-1   0    0    1   
$EndComp
Wire Wire Line
	8825 4200 8825 4400
$Comp
L fab:Power_GND #PWR012
U 1 1 60961010
P 8825 4600
F 0 "#PWR012" H 8825 4350 50  0001 C CNN
F 1 "Power_GND" H 8830 4427 50  0000 C CNN
F 2 "" H 8825 4600 50  0001 C CNN
F 3 "" H 8825 4600 50  0001 C CNN
	1    8825 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9425 3200 9975 3200
Text Label 9975 3200 2    50   ~ 0
UPDI
Wire Wire Line
	9425 3700 9950 3700
Wire Wire Line
	9425 3800 9950 3800
Text Label 9950 3700 2    50   ~ 0
RX
Text Label 9950 3800 2    50   ~ 0
TX
Wire Wire Line
	9425 3300 9975 3300
NoConn ~ 3625 1550
NoConn ~ 3625 1950
Wire Wire Line
	3625 1450 4100 1450
Wire Wire Line
	3625 1650 4025 1650
Wire Wire Line
	3625 1750 4425 1750
Wire Wire Line
	3625 1850 4425 1850
Text Label 4425 1750 2    50   ~ 0
TX
Text Label 4425 1850 2    50   ~ 0
RX
$Comp
L fab:Power_+5V #PWR04
U 1 1 60970391
P 4425 1650
F 0 "#PWR04" H 4425 1500 50  0001 C CNN
F 1 "Power_+5V" V 4440 1778 50  0000 L CNN
F 2 "" H 4425 1650 50  0001 C CNN
F 3 "" H 4425 1650 50  0001 C CNN
	1    4425 1650
	0    1    1    0   
$EndComp
$Comp
L fab:Power_GND #PWR03
U 1 1 60970AC5
P 4425 1450
F 0 "#PWR03" H 4425 1200 50  0001 C CNN
F 1 "Power_GND" V 4430 1322 50  0000 R CNN
F 2 "" H 4425 1450 50  0001 C CNN
F 3 "" H 4425 1450 50  0001 C CNN
	1    4425 1450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3625 2525 4425 2525
Wire Wire Line
	3625 2625 4425 2625
$Comp
L fab:Power_GND #PWR05
U 1 1 60975B02
P 4425 2625
F 0 "#PWR05" H 4425 2375 50  0001 C CNN
F 1 "Power_GND" V 4430 2497 50  0000 R CNN
F 2 "" H 4425 2625 50  0001 C CNN
F 3 "" H 4425 2625 50  0001 C CNN
	1    4425 2625
	0    -1   -1   0   
$EndComp
Text Label 4425 2525 2    50   ~ 0
UPDI
$Comp
L fab:C C1
U 1 1 609C9E91
P 1850 4800
F 0 "C1" H 1965 4846 50  0000 L CNN
F 1 "0.1uF" H 1965 4755 50  0000 L CNN
F 2 "fab:C_1206" H 1888 4650 50  0001 C CNN
F 3 "" H 1850 4800 50  0001 C CNN
	1    1850 4800
	1    0    0    -1  
$EndComp
$Comp
L fab:R R1
U 1 1 609C7E03
P 1400 4100
F 0 "R1" V 1193 4100 50  0000 C CNN
F 1 "1K" V 1284 4100 50  0000 C CNN
F 2 "fab:R_1206" V 1330 4100 50  0001 C CNN
F 3 "~" H 1400 4100 50  0001 C CNN
	1    1400 4100
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_GND #PWR02
U 1 1 609D9BF2
P 1400 6400
F 0 "#PWR02" H 1400 6150 50  0001 C CNN
F 1 "Power_GND" H 1405 6227 50  0000 C CNN
F 2 "" H 1400 6400 50  0001 C CNN
F 3 "" H 1400 6400 50  0001 C CNN
	1    1400 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6200 1400 6400
NoConn ~ 9425 3400
NoConn ~ 9425 3500
$Comp
L fab:Power_PWR_FLAG #FLG0101
U 1 1 60A1E9BF
P 4100 1450
F 0 "#FLG0101" H 4100 1525 50  0001 C CNN
F 1 "Power_PWR_FLAG" H 4100 1623 50  0000 C CNN
F 2 "" H 4100 1450 50  0001 C CNN
F 3 "~" H 4100 1450 50  0001 C CNN
	1    4100 1450
	1    0    0    -1  
$EndComp
Connection ~ 4100 1450
Wire Wire Line
	4100 1450 4425 1450
$Comp
L fab:Power_PWR_FLAG #FLG0102
U 1 1 60A1EB2B
P 4025 1650
F 0 "#FLG0102" H 4025 1725 50  0001 C CNN
F 1 "Power_PWR_FLAG" H 4025 1823 50  0000 C CNN
F 2 "" H 4025 1650 50  0001 C CNN
F 3 "~" H 4025 1650 50  0001 C CNN
	1    4025 1650
	1    0    0    -1  
$EndComp
Connection ~ 4025 1650
Wire Wire Line
	4025 1650 4425 1650
Wire Wire Line
	8825 2575 8825 2275
Connection ~ 8825 2575
Wire Wire Line
	7800 3025 7800 2575
Wire Wire Line
	7800 2575 8825 2575
Wire Wire Line
	7800 3325 7800 4400
Wire Wire Line
	7800 4400 8825 4400
Connection ~ 8825 4400
Wire Wire Line
	8825 4400 8825 4600
$Comp
L fab:Power_+5V #PWR011
U 1 1 60A2A2FB
P 8825 2275
F 0 "#PWR011" H 8825 2125 50  0001 C CNN
F 1 "Power_+5V" H 8840 2448 50  0000 C CNN
F 2 "" H 8825 2275 50  0001 C CNN
F 3 "" H 8825 2275 50  0001 C CNN
	1    8825 2275
	1    0    0    -1  
$EndComp
$Comp
L fab:LED D1
U 1 1 60A74CDA
P 1350 2275
F 0 "D1" V 1389 2157 50  0000 R CNN
F 1 "LED_PWR" V 1298 2157 50  0000 R CNN
F 2 "fab:LED_1206" H 1350 2275 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 1350 2275 50  0001 C CNN
	1    1350 2275
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R5
U 1 1 60A75231
P 1350 1650
F 0 "R5" H 1420 1696 50  0000 L CNN
F 1 "1K" H 1420 1605 50  0000 L CNN
F 2 "fab:R_1206" V 1280 1650 50  0001 C CNN
F 3 "~" H 1350 1650 50  0001 C CNN
	1    1350 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 2125 1350 1800
Wire Wire Line
	1350 2425 1350 2750
Wire Wire Line
	1350 1500 1350 1100
$Comp
L fab:Power_+5V #PWR013
U 1 1 60A8A7D5
P 1350 1100
F 0 "#PWR013" H 1350 950 50  0001 C CNN
F 1 "Power_+5V" H 1365 1273 50  0000 C CNN
F 2 "" H 1350 1100 50  0001 C CNN
F 3 "" H 1350 1100 50  0001 C CNN
	1    1350 1100
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_GND #PWR014
U 1 1 60A8B514
P 1350 2750
F 0 "#PWR014" H 1350 2500 50  0001 C CNN
F 1 "Power_GND" H 1355 2577 50  0000 C CNN
F 2 "" H 1350 2750 50  0001 C CNN
F 3 "" H 1350 2750 50  0001 C CNN
	1    1350 2750
	1    0    0    -1  
$EndComp
$Comp
L fab:R R2
U 1 1 6092320A
P 1400 4800
F 0 "R2" V 1193 4800 50  0000 C CNN
F 1 "1K" V 1284 4800 50  0000 C CNN
F 2 "fab:R_1206" V 1330 4800 50  0001 C CNN
F 3 "~" H 1400 4800 50  0001 C CNN
	1    1400 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 4650 1400 4450
Wire Wire Line
	1400 4450 1850 4450
Wire Wire Line
	1850 4450 1850 4650
Connection ~ 1400 4450
Wire Wire Line
	1400 4450 1400 4250
Wire Wire Line
	1850 4950 1850 5125
$Comp
L fab:Power_GND #PWR06
U 1 1 60934790
P 1850 5125
F 0 "#PWR06" H 1850 4875 50  0001 C CNN
F 1 "Power_GND" H 1855 4952 50  0000 C CNN
F 2 "" H 1850 5125 50  0001 C CNN
F 3 "" H 1850 5125 50  0001 C CNN
	1    1850 5125
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_+5V #PWR01
U 1 1 60938306
P 1400 3675
F 0 "#PWR01" H 1400 3525 50  0001 C CNN
F 1 "Power_+5V" H 1415 3848 50  0000 C CNN
F 2 "" H 1400 3675 50  0001 C CNN
F 3 "" H 1400 3675 50  0001 C CNN
	1    1400 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 3675 1400 3950
Wire Wire Line
	1400 4950 1400 5425
$Comp
L fab:C C2
U 1 1 60953EDA
P 2775 5425
F 0 "C2" H 2890 5471 50  0000 L CNN
F 1 "0.01uF" H 2890 5380 50  0000 L CNN
F 2 "fab:C_1206" H 2813 5275 50  0001 C CNN
F 3 "" H 2775 5425 50  0001 C CNN
	1    2775 5425
	0    1    1    0   
$EndComp
Wire Wire Line
	2625 5425 1400 5425
Connection ~ 1400 5425
Wire Wire Line
	1400 5425 1400 5600
Wire Wire Line
	2925 5425 3175 5425
$Comp
L fab:R R3
U 1 1 609588C9
P 3175 4875
F 0 "R3" V 2968 4875 50  0000 C CNN
F 1 "1M" V 3059 4875 50  0000 C CNN
F 2 "fab:R_1206" V 3105 4875 50  0001 C CNN
F 3 "~" H 3175 4875 50  0001 C CNN
	1    3175 4875
	1    0    0    -1  
$EndComp
$Comp
L fab:R R4
U 1 1 60958D3E
P 3175 6000
F 0 "R4" V 2968 6000 50  0000 C CNN
F 1 "1M" V 3059 6000 50  0000 C CNN
F 2 "fab:R_1206" V 3105 6000 50  0001 C CNN
F 3 "~" H 3175 6000 50  0001 C CNN
	1    3175 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3175 5025 3175 5425
Connection ~ 3175 5425
Wire Wire Line
	3175 5425 4450 5425
Wire Wire Line
	3175 5425 3175 5850
Wire Wire Line
	3175 4725 3175 4375
Wire Wire Line
	3175 6150 3175 6400
$Comp
L fab:Power_GND #PWR08
U 1 1 6095D2E8
P 3175 6400
F 0 "#PWR08" H 3175 6150 50  0001 C CNN
F 1 "Power_GND" H 3180 6227 50  0000 C CNN
F 2 "" H 3175 6400 50  0001 C CNN
F 3 "" H 3175 6400 50  0001 C CNN
	1    3175 6400
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_+5V #PWR07
U 1 1 6095F837
P 3175 4375
F 0 "#PWR07" H 3175 4225 50  0001 C CNN
F 1 "Power_+5V" H 3190 4548 50  0000 C CNN
F 2 "" H 3175 4375 50  0001 C CNN
F 3 "" H 3175 4375 50  0001 C CNN
	1    3175 4375
	1    0    0    -1  
$EndComp
$Comp
L fab:Power_+5V #PWR010
U 1 1 6095FFC4
P 4750 5100
F 0 "#PWR010" H 4750 4950 50  0001 C CNN
F 1 "Power_+5V" H 4765 5273 50  0000 C CNN
F 2 "" H 4750 5100 50  0001 C CNN
F 3 "" H 4750 5100 50  0001 C CNN
	1    4750 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5275 4750 5100
Wire Wire Line
	4750 5775 4750 5975
$Comp
L fab:Power_GND #PWR015
U 1 1 6096211A
P 4750 5975
F 0 "#PWR015" H 4750 5725 50  0001 C CNN
F 1 "Power_GND" H 4755 5802 50  0000 C CNN
F 2 "" H 4750 5975 50  0001 C CNN
F 3 "" H 4750 5975 50  0001 C CNN
	1    4750 5975
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5525 5575 5525
Wire Wire Line
	4450 5625 4175 5625
Wire Wire Line
	4175 5625 4175 6350
Wire Wire Line
	4175 6750 4625 6750
Connection ~ 5575 5525
Wire Wire Line
	5575 5525 6225 5525
$Comp
L fab:C C5
U 1 1 60973124
P 4775 6750
F 0 "C5" H 4890 6796 50  0000 L CNN
F 1 "100pF" H 4890 6705 50  0000 L CNN
F 2 "fab:C_1206" H 4813 6600 50  0001 C CNN
F 3 "" H 4775 6750 50  0001 C CNN
	1    4775 6750
	0    1    1    0   
$EndComp
$Comp
L fab:R R7
U 1 1 6097CB9C
P 4775 6350
F 0 "R7" V 4568 6350 50  0000 C CNN
F 1 "1M" V 4659 6350 50  0000 C CNN
F 2 "fab:R_1206" V 4705 6350 50  0001 C CNN
F 3 "~" H 4775 6350 50  0001 C CNN
	1    4775 6350
	0    1    1    0   
$EndComp
$Comp
L fab:R R6
U 1 1 6097D3E5
P 3800 6000
F 0 "R6" V 3593 6000 50  0000 C CNN
F 1 "10K" V 3684 6000 50  0000 C CNN
F 2 "fab:R_1206" V 3730 6000 50  0001 C CNN
F 3 "~" H 3800 6000 50  0001 C CNN
	1    3800 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 6150 3800 6600
Connection ~ 4175 6350
Wire Wire Line
	4175 6350 4175 6750
Wire Wire Line
	4175 6350 4625 6350
$Comp
L fab:C C4
U 1 1 60989E18
P 3800 6750
F 0 "C4" H 3915 6796 50  0000 L CNN
F 1 "1uF" H 3915 6705 50  0000 L CNN
F 2 "fab:C_1206" H 3838 6600 50  0001 C CNN
F 3 "" H 3800 6750 50  0001 C CNN
	1    3800 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6900 3800 7200
$Comp
L fab:Power_GND #PWR09
U 1 1 6099E2EC
P 3800 7200
F 0 "#PWR09" H 3800 6950 50  0001 C CNN
F 1 "Power_GND" H 3805 7027 50  0000 C CNN
F 2 "" H 3800 7200 50  0001 C CNN
F 3 "" H 3800 7200 50  0001 C CNN
	1    3800 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5575 5525 5575 6350
Wire Wire Line
	4925 6350 5575 6350
Connection ~ 5575 6350
Wire Wire Line
	5575 6350 5575 6750
Wire Wire Line
	4175 5625 3800 5625
Wire Wire Line
	3800 5625 3800 5850
Connection ~ 4175 5625
Wire Wire Line
	4925 6750 5575 6750
Text Label 6225 5525 2    50   ~ 0
AUDIO
Text Label 9975 3300 2    50   ~ 0
AUDIO
$EndSCHEMATC
